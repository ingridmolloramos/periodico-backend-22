import { Module } from '@nestjs/common';
import { BorradoresController } from './borradores.controller';
import { BorradoresService } from './borradores.service';

@Module({
  controllers: [BorradoresController],
  providers: [BorradoresService]
})
export class BorradoresModule {}
