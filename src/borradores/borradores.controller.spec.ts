import { Test, TestingModule } from '@nestjs/testing';
import { BorradoresController } from './borradores.controller';

describe('BorradoresController', () => {
  let controller: BorradoresController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BorradoresController],
    }).compile();

    controller = module.get<BorradoresController>(BorradoresController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
